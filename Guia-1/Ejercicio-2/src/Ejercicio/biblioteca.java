package Ejercicio;
import java.util.ArrayList;

public class biblioteca {
	
	ArrayList<String> titulo = new ArrayList<String>(60);
	ArrayList<String> autor = new ArrayList<String>(60);
	ArrayList<Integer> copias = new ArrayList<Integer>(60);
	ArrayList<Integer> copiasprestadas = new ArrayList<Integer>(60);
	

    biblioteca() {
		
	}
    
	libro cuaderno = new libro();
    
    public void añadir(String titulo,String autor,int copias,int copiasprestadas) {
    	this.titulo.add(titulo);
    	this.autor.add(autor);
    	this.copias.add(copias);
    	this.copiasprestadas.add(copiasprestadas);
    	
    }
    
    public int buscar(String titulolibro) {
    	int i = 0;
    	do {
    		i++;
    	}while(titulolibro != titulo.get(i) || i > 50);
    	
    	int nomli = i;
    	return nomli;
    }
    
    public void modificar(int identificar,String titulo,String autor,int copias,int copiasprestadas) {
    	this.titulo.set(identificar,titulo);
    	this.autor.set(identificar,autor);
    	this.copias.set(identificar,copias);
    	this.copiasprestadas.set(identificar,copiasprestadas);	
    }
    
    public String infotitulo(int identificar) {
    	String dtitulo = (String) this.titulo.get(identificar);
    	return dtitulo;
    }
    
    public String infoautor(int identificar) {
    	String dautor = (String) this.autor.get(identificar);
    	return dautor;
    }
    
    public int infocopias(int identificar) {
    	int dcopias = (int) this.copias.get(identificar);
    	return dcopias;
    }
    
    public int infocopiasprestadas(int identificar) {
    	int dcopiasprestadas = (int) this.copiasprestadas.get(identificar);
    	return dcopiasprestadas; 
	}
    
    public void eliminar(int identificar) {
    	this.titulo.remove(identificar);
    	this.autor.remove(identificar);
    	this.copias.remove(identificar);
    	this.copiasprestadas.remove(identificar);
    }

}
