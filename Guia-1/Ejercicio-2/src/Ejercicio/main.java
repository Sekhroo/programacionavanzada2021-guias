package Ejercicio;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		
		int iteracion = 1;
		String modititulo = "a";
		biblioteca registros = new biblioteca();
		
		do {
			System.out.println("Agregar un libro --------> (1)");
			System.out.println("Modificar un libro ------> (2)");
			System.out.println("Información de libro ----> (3)");
			System.out.println("Buscar un libro ---------> (4)");
			System.out.println("Dar de baja un libro ----> (5)");
			System.out.println("Proceso a realizar: ");
			int menu = sc.nextInt();
			
			if(menu == 1) {
				System.out.println("Titulo del libro");
				String ntitulo = sc.next();
				System.out.println("Autor del libro");
				String nautor = sc.next();
				System.out.println("Copias del libro");
				int ncopias = sc.nextInt();
				System.out.println("Copias prestadas del libro");
				int ncopiasprestadas = sc.nextInt();
				
				registros.añadir(ntitulo,nautor,ncopias,ncopiasprestadas);
				System.out.println("Se ha añadido con exito");
			}
			else if(menu == 2) {
				
				System.out.println("Titulo del libro a modifcar: ");
				String a = sc.next();
				modititulo = a;
				
				int nomli1 = registros.buscar(modititulo);
				
				System.out.println("Titulo del libro");
				String ntitulo = sc.next();
				System.out.println("Autor del libro");
				String nautor = sc.next();
				System.out.println("Copias del libro");
				int ncopias = sc.nextInt();
				System.out.println("Copias prestadas del libro");
				int ncopiasprestadas = sc.nextInt();
				
				registros.modificar(nomli1,ntitulo,nautor,ncopias,ncopiasprestadas);
				System.out.println("Se ha modificado con exito");
				
			}
			else if(menu == 3) {
				
				System.out.println("Titulo del que busca información: ");
				String vertitulo = sc.next();
				
				int nomli2 = registros.buscar(vertitulo);
				
				String dtitulo = registros.infotitulo(nomli2);
				System.out.println("Titulo: " + dtitulo);
				
				String dautor = registros.infoautor(nomli2);
				System.out.println("Autor: " + dautor);
				
				int dcopia = registros.infocopias(nomli2);
				System.out.println("Copias: " + dcopia);
				
				int dcopiasprestadas = registros.infocopiasprestadas(nomli2);
				System.out.println("Copias prestadas: " + dcopiasprestadas);
				
			}
			else if(menu == 4) {
				System.out.println("Titulo del que busca información: ");
				String buscalibro = sc.next();
				
				int nomli3 = registros.buscar(buscalibro);
				
				if(nomli3 > 50) {
					System.out.println("No se encuentra el libro");
				}else {
					System.out.println("Si se encuentra el libro");
				}
				
			}
			else if(menu == 5) {
				System.out.println("Titulo del libro que dara de baja: ");
				String deletelibro = sc.next();
				
				int nomli4 = registros.buscar(deletelibro);
				
				registros.eliminar(nomli4);
				
				System.out.println("Se ha eliminado con exito");
				
			}
			
			System.out.println("Continuar programa --> (1)");
			System.out.println("Cerrar programa -----> (2)");
			System.out.println("Proceso a realizar: ");
			int var = sc.nextInt();
			iteracion = var;
		}while(iteracion==1);

	}

}
