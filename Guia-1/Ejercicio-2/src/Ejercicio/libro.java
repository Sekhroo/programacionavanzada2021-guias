package Ejercicio;

public class libro {
    private String titulo;
    private String autor;
    private int copias;
    private int copiasprestadas;
    
    libro() {
    	this.titulo = "";
    	this.autor = "";
    	this.copias = 0;
    	this.copiasprestadas = 0;
    }
    
    //titulo
    public String gettitulo() {
    	return this.titulo;
    }
    public void settitulo(String nombre) {
        this.titulo = nombre;
    }
    
    //autor
    public String getautor() {
    	return this.autor;
    }
    public void setautor(String creador) {
        this.autor = creador;
    }
    
    //copias
    public int getcopias() {
    	return this.copias;
    }
    public void setcopias(int ejemplares) {
        this.copias = ejemplares;
    }
    
    //copias prestadas
    public int getcopiasprestadas() {
    	return this.copiasprestadas;
    }
    public void setcopiasprestadas(int ejemplaresprestados) {
        this.copiasprestadas = ejemplaresprestados;
    }
}
