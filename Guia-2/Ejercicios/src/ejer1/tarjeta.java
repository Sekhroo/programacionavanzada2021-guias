package ejer1;
import java.util.Scanner;

public class tarjeta {

    private String NombreTitular;
    private int Saldo;

    public tarjeta(){
        this.NombreTitular = "";
        this.Saldo = 0;
    }

    public String getNombreTitular() {
        return NombreTitular;
    }
    public void setNombreTitular(String nombreTitular) {
        NombreTitular = nombreTitular;
    }

    public int getSaldo() {
        return Saldo;
    }
    public void setSaldo(int saldo) {
        Saldo = saldo;
    }
}