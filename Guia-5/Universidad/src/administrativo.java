//SubClase de empleado
public class administrativo extends empleado{

	String seccion;
    
	//Constructor
    administrativo(String nombre, int ident, int selec, int añoincorp, int anexo) {
        super(nombre, ident, añoincorp, anexo);
        seleccionar(selec);
    }
    
    //Selecciona la seccion
    public void seleccionar(int selec) {
    	if (selec == 1) {
    		this.seccion = "Biblioteca";
    	}else if (selec == 2) {
    		this.seccion = "Decano";
    	}else if (selec == 3) {
    		this.seccion = "Secretaria";
    	}
    }
    
    //Getter y setter de la seccion
    public String getseccion() {
        return this.seccion;
    }
    public void setseccion(int selec) {
    	seleccionar(selec);
    }
}
