import java.util.Scanner;

public class busqueda {
	//====================================================================================================
	//--------------------------INSTANCIA DE OBJETOS------------------------------------------------------
	//====================================================================================================
	//Array de alumnos
	estudiante alumnos[] = new estudiante[100];
	//Array de profesores
	profesor profes[] = new profesor[100];
	//Array de administrativos
	administrativo admins[] = new administrativo[100];
	//Indice que indica el lugar de cada persona
	indice indice = new indice();

	
	
	//====================================================================================================
	//---------------------------FUNCION: PERSONAL BASE---------------------------------------------------
	//====================================================================================================
	//Funcion: generar alumno base
	public void genera_alumno_base() {
		String nombre_completo = "NOMBRE PRUEBA ESTUDIANTE 1";
		int ident = 123;
		int curso = 5;
		alumnos[indice.getindice_alumnos()] = new estudiante(nombre_completo, ident, curso);
		indice.setindice_alumnos(indice.getindice_alumnos()+1);
		
		nombre_completo = "NOMBRE PRUEBA ESTUDIANTE 2";
		ident = 1234;
		curso = 3;
		alumnos[indice.getindice_alumnos()] = new estudiante(nombre_completo, ident, curso);
		indice.setindice_alumnos(indice.getindice_alumnos()+1);
	}
	//Funcion: generar profesor base
	public void genera_profesor_base() {
		String nombre_completo = "NOMBRE PRUEBA PROFE 1";
		int ident = 123;
		int depart = 7;
		int añoincor = 2021;
		int anexo = 23;
		profes[indice.getindice_profes()] = new profesor(nombre_completo, ident, depart, añoincor, anexo);
		indice.setindice_profes(indice.getindice_profes()+1);
		
		nombre_completo = "NOMBRE PRUEBA PROFE 2";
		ident = 1234;
		depart = 1;
		añoincor = 2021;
		anexo = 24;
		profes[indice.getindice_profes()] = new profesor(nombre_completo, ident, depart, añoincor, anexo);
		indice.setindice_profes(indice.getindice_profes()+1);
	}
	//Funcion: generar administrativo base
	public void genera_administrativo_base() {
		String nombre_completo = "NOMBRE PRUEBA ADMINISTRATIVO 1";
		int ident = 123;
		int seccion = 3;
		int añoincor = 2021;
		int anexo = 23;
		admins[indice.getindice_admins()] = new administrativo(nombre_completo, ident, seccion, añoincor, anexo);
		indice.setindice_admins(indice.getindice_admins()+1);
		
		nombre_completo = "NOMBRE PRUEBA ADMINISTRATIVO 2";
		ident = 1234;
		seccion = 2;
		añoincor = 2021;
		anexo = 23;
		admins[indice.getindice_admins()] = new administrativo(nombre_completo, ident, seccion, añoincor, anexo);
		indice.setindice_admins(indice.getindice_admins()+1);
	}



	//====================================================================================================
	//---------------------------FUNCION: GENERAR PERSONAL -----------------------------------------------
	//====================================================================================================
	//Funcion: generar alumno
	public void genera_alumno(String nombre, int ident, int materia) {
		alumnos[indice.getindice_alumnos()] = new estudiante(nombre, ident, materia);
		indice.setindice_alumnos(indice.getindice_alumnos()+1);
	}
	//Funcion: generar profesor
	public void genera_profesor(String nombre, int ident, int depart) {
		Scanner sc=new Scanner(System.in);
		
		System.out.println("============== Pregunta ==============");
		System.out.println("Introduzca año del incorporacion: ");
		int añoincor = sc.nextInt();
		
		System.out.println("============== Pregunta ==============");
		System.out.println("Introduzca el anexo administrativo: ");
		int anexo = sc.nextInt();
		
		profes[indice.getindice_profes()] = new profesor(nombre, ident, depart, añoincor, anexo);
		indice.setindice_profes(indice.getindice_profes()+1);
	}
	//Funcion: generar administrativo
	public void genera_administrativo(String nombre, int ident, int seccion) {
		Scanner sc=new Scanner(System.in);
		
		System.out.println("============== Pregunta ==============");
		System.out.println("Introduzca año del incorporacion: ");
		int añoincor = sc.nextInt();
		
		System.out.println("============== Pregunta ==============");
		System.out.println("Introduzca el anexo administrativo: ");
		int anexo = sc.nextInt();
		
		admins[indice.getindice_admins()] = new administrativo(nombre, ident, seccion, añoincor, anexo);
		indice.setindice_admins(indice.getindice_admins()+1);
	}

	//====================================================================================================
	//---------------------------FUNCION: BUSCAR EL TOTAL DEL PERSONAL DE UN AREA ------------------------
	//====================================================================================================
	//Funcion: Total de alumnos
	public void consultar_alumnos_total() {
		for (int i = 0; i < indice.getindice_alumnos(); ++i) {
			System.out.println("=======================================");
			System.out.println("Nombre y Apellidos: " + alumnos[i].getnombre());
			System.out.println("N° de identificación: " + alumnos[i].getidentificacion());
		}
	}
	//Funcion: Total de profesores
	public void consultar_profesores_total() {
		for (int i = 0; i < indice.getindice_profes(); ++i) {
			System.out.println("=======================================");
			System.out.println("Nombre y Apellidos: " + profes[i].getnombre());
			System.out.println("N° de identificación: " + profes[i].getidentificacion());
		}
	}
	//Funcion: Total de administrativos
	public void consultar_administrativos_total() {
		for (int i = 0; i < indice.getindice_admins(); ++i) {
			System.out.println("=======================================");
			System.out.println("Nombre y Apellidos: " + admins[i].getnombre());
			System.out.println("N° de identificación: " + admins[i].getidentificacion());
		}
	}

	
	
	//====================================================================================================
	//---------------------------FUNCION: BUSCA PERSONA EN ESPECIFICO EN UN AREA -------------------------
	//====================================================================================================
	//Funcion: Busca un alumno especifico
	public void consultar_alumno() {
		Scanner sc=new Scanner(System.in);
		
		System.out.println("=======================================");
		System.out.println("¿Qué persona busca? (n° de identificación)");
		int b_ident = sc.nextInt();
		
		for (int i = 0; i < indice.getindice_alumnos(); ++i) {
			if (b_ident == alumnos[i].getidentificacion()) {
				System.out.println("=======================================");
				System.out.println("Nombre y Apellidos: " + alumnos[i].getnombre());
				System.out.println("N° de identificación: " + alumnos[i].getidentificacion());
				System.out.println("Curso matriculado: " + alumnos[i].getcurso());
			}
		}
	}
	//Funcion: Busca un profesor especifico
	public void consultar_profesor() {
		Scanner sc=new Scanner(System.in);
		
		System.out.println("=======================================");
		System.out.println("¿Qué persona busca? (n° de identificación)");
		int b_ident = sc.nextInt();
		
		for (int i = 0; i < indice.getindice_profes(); ++i) {
			if (b_ident == profes[i].getidentificacion()) {
				System.out.println("=======================================");
				System.out.println("Nombre y Apellidos: " + profes[i].getnombre());
				System.out.println("N° de identificación: " + profes[i].getidentificacion());
				System.out.println("Departamento: " + profes[i].getdepartamento());
				System.out.println("Año de incorporación: " + profes[i].getañoincorp());
				System.out.println("N° de anexo: " + profes[i].getanexo());
			}
		}
	}
	//Funcion: Busca un administrativo especifico
	public void consultar_administrativo(){
		Scanner sc=new Scanner(System.in);
		
		System.out.println("=======================================");
		System.out.println("¿Qué persona busca? (n° de identificación)");
		int b_ident = sc.nextInt();
		
		for (int i = 0; i < indice.getindice_admins(); ++i) {
			if (b_ident == admins[i].getidentificacion()) {
				System.out.println("=======================================");
				System.out.println("Nombre y Apellidos: " + admins[i].getnombre());
				System.out.println("N° de identificación: " + admins[i].getidentificacion());
				System.out.println("Sección: " + admins[i].getseccion());
				System.out.println("Año de incorporación: " + admins[i].getañoincorp());
				System.out.println("N° de anexo: " + admins[i].getanexo());
			}
		}
	}
}
