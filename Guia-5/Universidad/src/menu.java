import java.util.Scanner;

public class menu {
	
	//Instancia las funciones del programa
	busqueda funcion = new busqueda();
	
	//====================================================================================================
	//-----------------------------------Menu's del programa----------------------------------------------
	//====================================================================================================
	//Menu principal
	public int comandos_menu() {
		Scanner sc=new Scanner(System.in);
			
		System.out.println("================== Menu ===================");
		System.out.println("Registrar persona -------------------> [1]");
		System.out.println("Consultar todo personal -------------> [2]");
		System.out.println("Consultar detalles de una persona ---> [3]");
		System.out.println("Terminar programa -------------------> [0]");
		System.out.println("===========================================");
			
		int comando = sc.nextInt();
			
		return comando;
	}
	//Menu selecion estudiante, profesor o administrativo
	public int menu_seleccion_tipo() {
		Scanner sc=new Scanner(System.in);
				
		System.out.println("================ Menu =================");
		System.out.println("Estudiante ----------------------> [1]");
		System.out.println("Profesor ------------------------> [2]");
		System.out.println("Administrativo ------------------> [3]");
		System.out.println("=======================================");
			
		int selec = sc.nextInt();
				
		return selec;
	}
	//Menu selecion del curso o departamento de la persona
	public int menu_seleccion_materia() {
		Scanner sc=new Scanner(System.in);
				
		System.out.println("================ Menu =================");
		System.out.println("Bioinformática -------------------> [1]");
		System.out.println("Videojuegos ----------------------> [2]");
		System.out.println("Bioquimica -----------------------> [3]");
		System.out.println("Biotecnologia --------------------> [4]");
		System.out.println("Enfermeria -----------------------> [5]");
		System.out.println("Empresarial ----------------------> [6]");
		System.out.println("Derecho --------------------------> [7]");
		System.out.println("Computacion ----------------------> [8]");
		System.out.println("Mecanica -------------------------> [9]");
		System.out.println("Medicina ------------------------> [10]");
		System.out.println("=======================================");
			
		int selec = sc.nextInt();
				
		return selec;
	}
	//Menu selecion de seccion del administrativo
	public int menu_seleccion_seccion() {
		Scanner sc=new Scanner(System.in);
				
		System.out.println("================ Menu =================");
		System.out.println("Biblioteca -----------------------> [1]");
		System.out.println("Decano ---------------------------> [2]");
		System.out.println("Secretaria -----------------------> [3]");
		System.out.println("=======================================");
			
		int selec = sc.nextInt();
				
		return selec;
	}
	
	
	
	//====================================================================================================
	//------------------------------------Generar personas------------------------------------------------
	//====================================================================================================
	//Generar personal base
	public void gener_personal_base() {
		funcion.genera_alumno_base();
		funcion.genera_profesor_base();
		funcion.genera_administrativo_base();
	}
	//Generacion del personas especificas
	public void gener_personal() {
		Scanner sc=new Scanner(System.in);
		
		System.out.println("============== Pregunta ==============");
		System.out.println("Introduzca nombre completo de la persona: ");
		String nombre = sc.nextLine();
		
		System.out.println("============== Pregunta ==============");
		System.out.println("Introduzca n° de identificación: ");
		int ident = sc.nextInt();
		
		System.out.println("============== Pregunta ==============");
		System.out.println("¿Qué tipo de personal generará? ");
		int selec = menu_seleccion_tipo();
		
		if (selec == 1) {
			System.out.println("============== Pregunta ==============");
			System.out.println("¿En que curso se matriculo el alumno? ");
			selec = menu_seleccion_materia();
			funcion.genera_alumno(nombre,selec,ident);
		}else if (selec == 2) {
			System.out.println("============== Pregunta ==============");
			System.out.println("¿En que departamento trabaja el profesor? ");
			selec = menu_seleccion_materia();
			funcion.genera_profesor(nombre,selec,ident);
		}else if (selec == 3) {
			System.out.println("============== Pregunta ==============");
			System.out.println("¿En que sección administrativa trabaja? ");
			selec = menu_seleccion_seccion();
			funcion.genera_administrativo(nombre,selec,ident);
		}
 	}
	

	
	//====================================================================================================
	//-----------------------------------Comandos del programa--------------------------------------------
	//====================================================================================================
	//Consultar todo el personal
	public void consultar_todo_personal() {
		int selec = menu_seleccion_tipo();
		if(selec == 1) {
			funcion.consultar_alumnos_total();
		}else if(selec == 2){
			funcion.consultar_profesores_total();
		}else if(selec == 3){
			funcion.consultar_administrativos_total();
		}
	}
	//Buscar una persona especifica
	public void muestra_detalle_personal() {
		int selec = menu_seleccion_tipo();
		if(selec == 1) {
			funcion.consultar_alumno();
		}else if(selec == 2){
			funcion.consultar_profesor();
		}else if(selec == 3){
			funcion.consultar_administrativo();
		}
	}
}	
