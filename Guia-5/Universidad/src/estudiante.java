//SubClase de persona
public class estudiante extends persona {
	
	String curso;
	
	//Constructor
	estudiante(String nombre, int ident, int selec) {
        super(nombre, ident);
        seleccionar(selec);
    }
	
	 //Selecciona la seccion
    public void seleccionar(int selec) {
    	if (selec == 1) {
    		this.curso = "Bioinformática";
    	}else if (selec == 2) {
    		this.curso = "Videojuegos";
    	}else if (selec == 3) {
    		this.curso = "Bioquimica";
    	}else if (selec == 4) {
    		this.curso = "Biotecnologia";
    	}else if (selec == 5) {
    		this.curso = "Enfermeria";
    	}else if (selec == 6) {
    		this.curso = "Empresarial";
    	}else if (selec == 7) {
    		this.curso = "Derecho";
    	}else if (selec == 8) {
    		this.curso = "Computacion";
    	}else if (selec == 9) {
    		this.curso = "Mecanica";
    	}else if (selec == 10) {
    		this.curso = "Medicina";
    	}
    }
    
    //Getter y setter del curso
    public String getcurso() {
        return this.curso;
    }
    public void setcurso(int selec) {
    	seleccionar(selec);
    } 
}
