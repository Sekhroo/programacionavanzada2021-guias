//SuperClase persona
public class persona {
	
	private String nombre;
	private int ident;
	
	//Constructor
    public persona(String nombre, int ident){
    	this.nombre = nombre;
    	this.ident = ident;
    }
    
    //Getter y setter del nombre de la persona
    public String getnombre() {
        return this.nombre;
    }
    public void setnombre(String nombre) {
    	this.nombre = nombre;	
    }
    
    //Getter y setter del n° de identificacion de la persona
    public int getidentificacion() {
    	return this.ident;
    }
    public void setidentificacion(int ident) {
    	this.ident = ident;
    }
}
