
public class indice {

	private int indice_alumnos = 0;
	private int indice_profes = 0;
	private int indice_admins = 0;
	
	//Getter y Setter: Alumno
	public int getindice_alumnos() {
		return this.indice_alumnos;
	}
	public void setindice_alumnos(int indice_alumnos) {
		this.indice_alumnos = indice_alumnos;
	}
	
	//Getter y Setter: profesor
	public int getindice_profes() {
		return this.indice_profes;
	}
	public void setindice_profes(int indice_profes) {
		this.indice_profes = indice_profes;
	}
	
	//Getter y Setter: administrativo
	public int getindice_admins() {
		return this.indice_admins;
	}
	public void setindice_admins(int indice_admins) {
		this.indice_admins = indice_admins;
	}
}
