//Clase main
public class main {

	public static void main(String[] args) {
		
		int directriz;
		
		menu menu = new menu();
		menu.gener_personal_base();
		
		//Programa y funciones
		do {
			directriz = menu.comandos_menu();
			if(directriz == 1) {
				menu.gener_personal();
			}else if(directriz == 2) {
				menu.consultar_todo_personal();
			}else if(directriz == 3) {
				menu.muestra_detalle_personal();
			}
		}while(directriz != 0);
	}
}
