//SubClase de empleado
public class profesor extends empleado{
	
	String depart;
    
	//Constructor
    profesor(String nombre, int ident, int selec, int añoincorp, int anexo) {
        super(nombre, ident, añoincorp, anexo);
        seleccionar(selec);
    }
    
    //Selecciona la seccion
    public void seleccionar(int selec) {
    	if (selec == 1) {
    		this.depart = "Bioinformática";
    	}else if (selec == 2) {
    		this.depart = "Videojuegos";
    	}else if (selec == 3) {
    		this.depart = "Bioquimica";
    	}else if (selec == 4) {
    		this.depart = "Biotecnologia";
    	}else if (selec == 5) {
    		this.depart = "Enfermeria";
    	}else if (selec == 6) {
    		this.depart = "Empresarial";
    	}else if (selec == 7) {
    		this.depart = "Derecho";
    	}else if (selec == 8) {
    		this.depart = "Computacion";
    	}else if (selec == 9) {
    		this.depart = "Mecanica";
    	}else if (selec == 10) {
    		this.depart = "Medicina";
    	}
    }
    
    //Getter y setter del departamento
    public String getdepartamento() {
        return this.depart;
    }
    public void setdepartamento(int selec) {
    	seleccionar(selec);
    } 
}
