//SubClase de persona y SuperClase de los empleados
public class empleado extends persona{
	
	int añoincorp;
    int anexo;
    	
    //Constructor
    empleado(String nombre, int ident, int añoincorp, int anexo) {
    	super(nombre,ident);
        this.añoincorp = añoincorp;
        this.anexo = anexo;
    }
    
    //Getter y setter del año de incorporacion del empleado
    public int getañoincorp() {
        return this.añoincorp;
    }
    public void setañoincorp(int añoincorp) {
    	this.añoincorp = añoincorp;	
    }
    
    //Getter y setter del anexo del empleado
    public int getanexo() {
    	return this.anexo;
    }
    public void setanexo(int anexo) {
    	this.anexo = anexo;
    }

}
