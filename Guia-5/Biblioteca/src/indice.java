
public class indice {

	private int indice_libro = 0;
	private int indice_revista = 0;
	
	public int getindice_libro() {
		return this.indice_libro;
	}
	public void setindice_libro(int indice_libro) {
		this.indice_libro = indice_libro;
	}
	
	public int getindice_revista() {
		return this.indice_revista;
	}
	public void setindice_revista(int indice_revista) {
		this.indice_revista = indice_revista;
	}
}
