import java.util.Scanner;

public class menu {
	
	//Instancia las funciones del programa
	manipular_obj funcion = new manipular_obj();
	
	//====================================================================================================
	//-----------------------------------Menu's del programa----------------------------------------------
	//====================================================================================================
	//Menu principal
	public int comandos_menu() {
		funcion.cobrar();
		Scanner sc=new Scanner(System.in);
			
		System.out.println("================ Menu =================");
		System.out.println("Añadir --------------------------> [1]");
		System.out.println("Consultar titulos ---------------> [2]");
		System.out.println("Consultar detalles del titulo ---> [3]");
		System.out.println("Pedir ---------------------------> [4]");
		System.out.println("Entregar ------------------------> [5]");
		System.out.println("Consultar recibo ----------------> [6]");
		System.out.println("Cambiar usuario -----------------> [7]");
		System.out.println("Terminar programa ---------------> [0]");
		System.out.println("=======================================");
			
		int comando = sc.nextInt();
			
		return comando;
	}
	//Menu selecion libro o revista
	public int menu_seleccion() {
		Scanner sc=new Scanner(System.in);
			
		System.out.println("================ Menu =================");
		System.out.println("Libro --------------------------> [1]");
		System.out.println("Revista ------------------------> [2]");
		System.out.println("=======================================");
			
		int selec = sc.nextInt();
			
		return selec;
	}

	
	
	//====================================================================================================
	//--------------------------------------Comandos usuario----------------------------------------------
	//====================================================================================================
	//Generacion del usuario
	public void usuario() {
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Nombre de usuario: ");
		
		String usuario = sc.nextLine();
		
		funcion.establece_usuario(usuario);	
	}
	//Otorga recibo del usuario
	public void dimerecibo() {
		funcion.getrecibo();
	}
	
	
	
	//====================================================================================================
	//-------------------------------------------Archivero------------------------------------------------
	//====================================================================================================
	//Generar objetos base
	public void gener_obj_base() {
		funcion.genera_libro_base();
		funcion.genera_revista_base();
	}
	//Genera un obj libro o revistta en el archivero
	public void gener_obj() {
		int selec = menu_seleccion();
		if(selec == 1) {
			funcion.genera_libro();
		}else if(selec == 2){
			funcion.genera_revista();
		}
	}
	//Muestra todos los detalles de X
	public void muestra_detalle() {
		int selec = menu_seleccion();
		if(selec == 1) {
			funcion.buscar_libro();
		}else if(selec == 2){
			funcion.buscar_revista();
		}
	}
	//Listado de todo lo almacenado en el archivero (titulos y estado)
	public void consultar_titulos() {
		int selec = menu_seleccion();
		if(selec == 1) {
			funcion.consultar_libro();
		}else if(selec == 2){
			funcion.consultar_revista();
		}
	}
	
	
	
	//====================================================================================================
	//------------------------------Funciones de pedir y entregar-----------------------------------------
	//====================================================================================================
	//Pedir
	public void pedir() {
		int selec = menu_seleccion();
		if(selec == 1) {
			funcion.pedir_libro();
		}else if(selec == 2){
			funcion.pedir_revista();
		}
	}
	//Engregar
	public void entrega() {
		int selec = menu_seleccion();
		if(selec == 1) {
			funcion.entrega_libro();
		}else if(selec == 2){
			funcion.entrega_revista();
		}
	}
}
