import java.util.Scanner;


public class manipular_obj {
	//====================================================================================================
	//--------------------------INSTANCIA DE OBJETOS------------------------------------------------------
	//====================================================================================================
	persona usuario = new persona();
	indice indice = new indice();
	libro libros[] = new libro[100];
	revista revistas[] = new revista[100];
	
	
	
	//====================================================================================================
	//---------------------------FUNCIONES USUARIO--------------------------------------------------------
	//====================================================================================================
	public void establece_usuario(String nombre) {
		usuario.setnombrepersona(nombre);
		usuario.setrecibo(0);
	}
	public void getrecibo() {
		System.out.println("=======================================");
		System.out.println("Debe: "+ usuario.getrecibo());
	}
	
	
	
	//====================================================================================================
	//---------------------------FUNCIONES LIBRO----------------------------------------------------------
	//====================================================================================================
	public void genera_libro_base() {
		String titulo = "nacidos de la bruma";
		int codigo = 1;
		int año = 2021;
		libros[indice.getindice_libro()] = new libro(titulo, codigo,año);
		indice.setindice_libro(indice.getindice_libro()+1);
		
		titulo = "todo es numero";
		codigo = 2;
		año = 2021;
		libros[indice.getindice_libro()] = new libro(titulo, codigo,año);
		indice.setindice_libro(indice.getindice_libro()+1);
		
		titulo = "shingeki no kiojin";
		codigo = 3;
		año = 2021;
		libros[indice.getindice_libro()] = new libro(titulo, codigo,año);
		indice.setindice_libro(indice.getindice_libro()+1);
	}
	public void genera_libro() {
		Scanner sc=new Scanner(System.in);
		System.out.println("=======================================");
		System.out.println("Titulo: ");
		String titulo = sc.nextLine();
		System.out.println("Código: ");
		int codigo = sc.nextInt();
		System.out.println("Año de publicación: ");
		int año = sc.nextInt();
		libros[indice.getindice_libro()] = new libro(titulo,codigo,año);
		indice.setindice_libro(indice.getindice_libro()+1);
	}
	public void consultar_libro() {
		for (int i = 0; i < indice.getindice_libro(); ++i) {
			System.out.println("=======================================");
			muestra_detalle_libro(i);
		}
	}
	public void buscar_libro() {
		Scanner sc=new Scanner(System.in);
		System.out.println("=======================================");
		System.out.println("¿Qué libro busca? (Código)");
		int b_codigo = sc.nextInt();
		for (int i = 0; i < indice.getindice_libro(); ++i) {
			if (b_codigo == libros[i].getcodigo()) {
				System.out.println("=======================================");
				muestra_detalle_libro(i);
			}
		}
	}
	public void muestra_detalle_libro(int indice_buscado) {
		System.out.println("Titulo: " + libros[indice_buscado].gettitulo());
		System.out.println("Código: " + libros[indice_buscado].getcodigo());
		System.out.println("Año de publicacion: " + libros[indice_buscado].getaño());
		String estado = traduce_estado(libros[indice_buscado].getestado());
		System.out.println("Estado: " + estado);
	}
	public void pedir_libro() {
		Scanner sc=new Scanner(System.in);
		System.out.println("=======================================");
		System.out.println("¿Qué libro busca? (Código)");
		int b_codigo = sc.nextInt();
		for (int i = 0; i < indice.getindice_libro(); ++i) {
			if (b_codigo == libros[i].getcodigo()) {
				if (!libros[i].getestado()) {
					System.out.println("=======================================");
					System.out.println("El libro " + revistas[i].gettitulo() + " fue prestado");
					libros[i].prestar();
					libros[i].prestado_libro();
				}
			}
		}
	}
	public void entrega_libro() {
		Scanner sc=new Scanner(System.in);
		System.out.println("=======================================");
		System.out.println("¿Qué libro entregará? (Código)");
		int b_codigo = sc.nextInt();
		for (int i = 0; i < indice.getindice_libro(); ++i) {
			if (b_codigo == libros[i].getcodigo()) {
				if (libros[i].getestado()) {
					System.out.println("=======================================");
					System.out.println("El libro " + libros[i].gettitulo() + " fue entregado");
					libros[i].devolver();
				}else {
					System.out.println("El libro ya esta en el archivero");
				}
			}
		}
	}
	
	
	
	//====================================================================================================
	//---------------------------FUNCIONES REVISTA--------------------------------------------------------
	//====================================================================================================
	public void genera_revista_base() {
		String titulo = "tokyo ghoul";
		int codigo = 1;
		String genero = "comic";
		int año = 2021;
		revistas[indice.getindice_revista()] = new revista(titulo,codigo,genero,año);
		indice.setindice_revista(indice.getindice_revista()+1);
		
		titulo = "dr stone";
		codigo = 2;
		genero = "ciencia";
		año = 2021;
		revistas[indice.getindice_revista()] = new revista(titulo,codigo,genero,año);
		indice.setindice_revista(indice.getindice_revista()+1);
		
		titulo = "colocolo";
		codigo = 3;
		genero = "deporte";
		año = 2021;
		revistas[indice.getindice_revista()] = new revista(titulo,codigo,genero,año);
		indice.setindice_revista(indice.getindice_revista()+1);
		
		titulo = "chile";
		codigo = 4;
		genero = "gastronomia";
		año = 2021;
		revistas[indice.getindice_revista()] = new revista(titulo,codigo,genero,año);
		indice.setindice_revista(indice.getindice_revista()+1);
	}
	public void genera_revista() {
		Scanner sc=new Scanner(System.in);		
		System.out.println("=======================================");
		System.out.println("Titulo: ");
		String titulo = sc.nextLine();
		System.out.println("Genero (comic, ciencia, deporte o gastronomia): ");
		String genero = sc.nextLine();
		System.out.println("Código: ");
		int codigo = sc.nextInt();
		System.out.println("Año de publicación: ");
		int año = sc.nextInt();	
		revistas[indice.getindice_revista()] = new revista(titulo,codigo,genero,año);
		indice.setindice_revista(indice.getindice_revista()+1);
	}
	public void consultar_revista() {
		for (int i = 0; i < indice.getindice_revista(); ++i) {
			System.out.println("=======================================");
			muestra_detalle_revista(i);
		}
	}
	public void buscar_revista() {
		Scanner sc=new Scanner(System.in);
		System.out.println("=======================================");
		System.out.println("¿Qué revista busca? (Código)");
		int b_codigo = sc.nextInt();
		for (int i = 0; i < indice.getindice_revista(); ++i) {
			if (b_codigo == revistas[i].getcodigo()) {
				System.out.println("=======================================");
				muestra_detalle_revista(i);
			}
		}
	}
	public void muestra_detalle_revista(int indice_buscado) {
		System.out.println("Titulo: " + revistas[indice_buscado].gettitulo());
		System.out.println("Código: " + revistas[indice_buscado].getcodigo());
		System.out.println("Genero: " + revistas[indice_buscado].getgenero());
		System.out.println("Año de publicacion: " + revistas[indice_buscado].getaño());
		String estado = traduce_estado(revistas[indice_buscado].getestado());
		System.out.println("Estado: " + estado);	
	}
	public void pedir_revista() {
		Scanner sc=new Scanner(System.in);
		System.out.println("=======================================");
		System.out.println("¿Qué revista busca? (Código)");
		int b_codigo = sc.nextInt();
		for (int i = 0; i < indice.getindice_revista(); ++i) {
			if (b_codigo == revistas[i].getcodigo()) {
				if (!revistas[i].getestado()) {
					if ("comic" == revistas[i].getgenero()) {
						System.out.println("=======================================");
						System.out.println("La revista " + revistas[i].gettitulo() + " fue prestada");
						revistas[i].prestar();
						revistas[i].prestado_comic();
					}else if ("ciencia" == revistas[i].getgenero()) {
						System.out.println("=======================================");
						System.out.println("La revista " + revistas[i].gettitulo() + " fue prestada");
						revistas[i].prestar();
						revistas[i].prestado_science();
					}else if ("deporte" == revistas[i].getgenero() || "gastronomia" == revistas[i].getgenero()){
						System.out.println("=======================================");
						System.out.println("La revista " + revistas[i].gettitulo() + " fue prestada");
						revistas[i].prestar();
						revistas[i].prestado_depor_gastr();
					}
				}
			}
		}
	}
	public void entrega_revista() {
		Scanner sc=new Scanner(System.in);
		System.out.println("=======================================");
		System.out.println("¿Qué revista entregará? (Código)");
		int b_codigo = sc.nextInt();
		for (int i = 0; i < indice.getindice_revista(); ++i) {
			if (b_codigo == revistas[i].getcodigo()) {
				if (revistas[i].getestado()) {
						revistas[i].devolver();
						System.out.println("=======================================");
						System.out.println("La revista " + revistas[i].gettitulo() + " fue entregado");
				}else {
					System.out.println("La revista ya esta en el archivero");
				}
			}
		}
	}
	
	
	
	//====================================================================================================
	//-------------------------FUNCIONES POLIVALENTE------------------------------------------------------
	//====================================================================================================
	public String traduce_estado(boolean estado) {
		String respuesta = "";
		if (estado) {
			respuesta = "prestado";
		}else if(!estado) {
			respuesta = "disponible";
		}
		return respuesta;
	}
	public void cobrar() {
		for (int i = 0; i < indice.getindice_revista(); ++i) {
			int selec = revistas[i].prestado();
			if (selec == 1) {
				usuario.setrecibo(usuario.getrecibo()+1290);
				revistas[i].settitulo(revistas[i].gettitulo() + " (Perdida)");
			}
		}
		for (int i = 0; i < indice.getindice_libro(); ++i) {
			int selec = libros[i].prestado();
			if (selec == 1) {
				usuario.setrecibo(usuario.getrecibo()+1290);	
			}
		}
	}
}
