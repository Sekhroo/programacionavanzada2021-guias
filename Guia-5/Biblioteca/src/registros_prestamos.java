import java.time.LocalDate;

public class registros_prestamos {
	
	private LocalDate date_now = LocalDate.now();
	private LocalDate date_limite_libro;
	private LocalDate date_limite_revis_comic;
	private LocalDate date_limite_revis_science;
	private LocalDate date_limite_revis_deporgastr;
	
	public void date_suma_libro() {
		this.date_limite_libro = this.date_now.minusDays(5);
	}
	public void date_suma_revista_comic() {
		this.date_limite_revis_comic = this.date_now.minusDays(1);
	}
	public void date_suma_revista_cientifica() {
		this.date_limite_revis_science = this.date_now.minusDays(2);
	}
	public void date_suma_revista_deportivogastronomico() {
		this.date_limite_revis_deporgastr = this.date_now.minusDays(3);
	}
	
	//Getters y setters: date
	public LocalDate getdate_actual() {
		return this.date_now;
	}
	public LocalDate getdate_limite() {
		return this.date_limite_libro;
	}
	public LocalDate getdate_limite_revis_comic() {
		return this.date_limite_revis_comic;
	}
	public LocalDate getdate_limite_revis_science() {
		return this.date_limite_revis_science;
	}
	public LocalDate getdate_limite_revis_deporgastr() {
		return this.date_limite_revis_deporgastr;
	}
}
