import java.time.LocalDate;

public class libro implements interfaz {
	
	private String titulo;
	private int codigo;
	private int año;
	private boolean estado;
	private LocalDate limite = LocalDate.now().plusDays(1000);
	private registros_prestamos regis_pres;
	
	private int indice;
	
	registros_prestamos date_local = new registros_prestamos();
	
	public libro(String titulo, int codigo, int año) {
		this.titulo = titulo;
		this.codigo = codigo;
		this.año = año;
		this.estado = false;
		this.indice = 0;
	}
	
	@Override
	public void prestar() {
		this.estado = true;
	}

	@Override
	public void devolver() {		
		this.estado = false;
	}

	@Override
	public int prestado() {
		int si = 0;
		if (date_local.getdate_actual().minusDays(1) == this.limite) {
			si = 1;
		}
		return 	si;
	}
	
	public void prestado_libro() {
		this.limite = date_local.getdate_limite();
	}
	

	//Getters y setters: titulo
	public String gettitulo() {
		return this.titulo;
	}
	public void settitulo(String titulo) {
		this.titulo = titulo;
	}
	
	//Getters y setters: código
	public int getcodigo() {
		return this.codigo;
	}
	public void setcodigo(int codigo) {
		this.codigo = codigo;
	}
	
	//Getters y setters: Año de publicación
	public int getaño() {
		return this.año;
	}
	public void setaño(int año) {
		this.año = año;
	}
	
	//Getters y setters: disponibilidad
	public boolean getestado() {
		return this.estado;
	}
	public void setestado(boolean estado) {
		this.estado = estado;
	}
	
	//Getters y setters: LocalDate
	public registros_prestamos getregis_date() {
		return this.regis_pres;
	}
	public LocalDate getlimite() {
		return this.limite;
	}
	public void setlimite(LocalDate limite) {
		this.limite = limite;
	}
}
