import java.time.LocalDate;

public class revista implements interfaz {
	
	private String titulo;
	private String genero;
	private int codigo;
	private int año;
	private boolean estado;
	private LocalDate limite = LocalDate.now().plusDays(1000);
	private registros_prestamos regis_pres;
	
	private int indice;
	
	registros_prestamos date_local = new registros_prestamos();
	
	public revista(String titulo, int codigo, String genero, int año) {
		this.titulo = titulo;
		this.genero = genero;
		this.codigo = codigo;
		this.año = año;
		this.estado = false;
		this.indice = 0;
	}
	
	@Override
	public void prestar() {
		this.estado = true;
	}

	@Override
	public void devolver() {
		this.estado = false;
	}

	@Override
	public int prestado() {	
		int si = 0;
		if (date_local.getdate_actual().minusDays(1) == this.limite) {
			si = 1;
		}
		return 	si;
	}
	
	public void prestado_comic() {
		this.limite = date_local.getdate_limite_revis_comic();
	}
	public void prestado_science() {
		this.limite = date_local.getdate_limite_revis_science();
	}
	public void prestado_depor_gastr() {
		this.limite = date_local.getdate_limite_revis_deporgastr();
	}
	
	//Getters y setters: titulo
	public String gettitulo() {
		return this.titulo;
	}
	public void settitulo(String titulo) {
		this.titulo = titulo;
	}
	
	//Getters y setters: código
	public int getcodigo() {
		return this.codigo;
	}
	public void setcodigo(int codigo) {
		this.codigo = codigo;
	}
	
	//Getters y setters: genero
	public String getgenero() {
		return this.genero;
	}
	public void setgenero(String genero) {
		this.genero = genero;
	}
	
	//Getters y setters: Año de publicación
	public int getaño() {
		return this.año;
	}
	public void setaño(int año) {
		this.año = año;
	}
	
	//Getters y setters: disponibilidad
	public boolean getestado() {
		return this.estado;
	}
	public void setestado(boolean estado) {
		this.estado = estado;
	}
	
	//Getters y setters: LocalDate
	public registros_prestamos getregis_date() {
		return this.regis_pres;
	}
	public LocalDate getlimite() {
		return this.limite;
	}
	public void setlimite(LocalDate limite) {
		this.limite = limite;
	}
}
