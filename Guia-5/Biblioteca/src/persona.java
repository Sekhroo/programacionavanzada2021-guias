
public class persona {
	
	private String usuario;
	private int recibo = 0;
	
	//Constructor
    public persona(){
    	this.usuario = "";
    	this.recibo = 0;
    }
    
    //Getter y setter del usuario
    public String getnombreusuario() {
        return this.usuario;
    }
    public void setnombrepersona(String usuario) {
    	this.usuario = usuario;	
    }
    
    //Getter y setter del recibo del usuario
    public int getrecibo() {
    	return this.recibo;
    }
    public void setrecibo(int recibo) {
    	this.recibo = recibo;
    }
}
