package objetos;

public class velocimetro {
	
	// Velocidad refiere a velocidad actual del automovil
	private int velocidad;
	private int velocidad_maxima = 120;
	
	
	// ------------------------------------------------------------
	// Getters y Setters
	// ------------------------------------------------------------
	public int getvelocidad() {
		return velocidad;
	}
	public void setvelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	// ------------------------------------------------------------
	public int getvelocidadmaxima() {
		return this.velocidad_maxima;
	}
	// ------------------------------------------------------------
}
