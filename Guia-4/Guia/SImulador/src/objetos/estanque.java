package objetos;

public class estanque {

	private Double volumen = 32.0;
	private Double volumen_max = 32.0;
	
	// ------------------------------------------------------------
	// Getters y Setters
	// ------------------------------------------------------------
	public Double getvolumen() {
		return volumen;
	}
	public void setvolumen(Double volumen) {
		this.volumen = volumen;
	}
	
	public Double getvolumen_max() {
		return volumen_max;
	}
	// ------------------------------------------------------------
}
